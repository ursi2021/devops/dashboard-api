import {Group} from "../models/group.model";
import eventEmitter from "events"
import {logger} from "../loaders/winston";
import {exec} from "child_process"


export default class CoverageService {

    constructor() {
        Group.findOrCreate({where: {name: "back-office-magasin-api"}});
        Group.findOrCreate({where: {name: "e-commerce-api"}});
        Group.findOrCreate({where: {name: "gestion-entrepots-api"}});
        Group.findOrCreate({where: {name: "referentiel-produit-api"}});
        Group.findOrCreate({where: {name: "caisse-api"}});
        Group.findOrCreate({where: {name: "gestion-promotion-api"}});
        Group.findOrCreate({where: {name: "relation-client-api"}});
        Group.findOrCreate({where: {name: "decisionnel-api"}});
        Group.findOrCreate({where: {name: "gestion-commerciale-api"}});
        Group.findOrCreate({where: {name: "monetique-paiement-api"}});
    }

    public async coverage() {
        logger.info("[coverage] : start sync.")
        Group.findAll().then((groups: Group[]) => {
            groups.forEach((group: Group) => {
                this.updateCoverage(group.name)
            })
        })
    }

    public async updateCoverage(repo: string) {
        logger.info("[" + repo + "] : start update.")
        exec('cd all_apps/' + repo + ' ; git pull ', (err, stdout, stderr) => {
                if (err) {
                    //some err occurred
                    logger.error(err);
                } else {
                    exec('cd all_apps/' + repo + ' ;npm install', (err, stdout, stderr) => {
                            if (err) {
                                //some err occurred
                                logger.error(err);
                            } else {
                                exec('cd all_apps/' + repo + ' ; nyc npm test | grep \'All files\' | cut -d \'|\' -f4 ', (err, stdout, stderr) => {
                                        if (err) {
                                            //some err occurred
                                            logger.error(err);
                                        } else {
                                            // the *entire* stdout and stderr (buffered)
                                            const result = stdout.replace(/\s/g, '');
                                            const value = parseFloat(result);
                                            Group.findOne({where: {name: repo}}).then((group: Group) => {
                                                logger.info("[" + repo + "] : " + value);
                                                group.coverage = value;
                                                group. save()
                                            });
                                        }
                                    }
                                );
                            }
                        }
                    );
                }
            }
        )
    }
}

