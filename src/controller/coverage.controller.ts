import ProductService from "../service/coverage.service";
import {Request, Response} from "express";
import {logger} from "../loaders/winston";
import CoverageService from "../service/coverage.service";
import {Group} from "../models/group.model";

export class CoverageController {

    public static getCoverage(req: Request, res: Response) {
        Group.findAll().then((groups: Group[]) => {
            res.status(200).json(groups);
        }).catch((error: Error) => {
            res.status(500).json(error);
        })
    }

    public static updateCoverage(req: Request, res: Response) {
        new CoverageService().coverage();
        res.status(200);
    }
}