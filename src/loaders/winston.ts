import Winston, {silly} from 'winston'
import dateFormat from 'dateformat'



export const logger = Winston.createLogger(
    {
        level: 'info',
        format: Winston.format.combine(
            Winston.format.colorize(),
            Winston.format.timestamp(),
            Winston.format.printf(info => dateFormat(info.timestamp,"yyyy-mm-dd h:MM:ss") + ` [${info.level}]: ${info.message}`),
        ),
        transports: [
            new Winston.transports.Console()
        ]
    })
