import expressLoader from './express';
import {logger} from "./winston";
import sequelizeLoader from './sequelize';
import {Express} from "express";

export default class Loader {
    public app: Express

    constructor(app: Express) {
        this.app = app
    }

    public init(): Express {
        sequelizeLoader()
            .then(() => {
                logger.info('Sequelize Intialized');
            })
            .catch(() => {
                logger.error('Sequelize Intialization Failed');
            });

        try {
            this.app = expressLoader(this.app);
            logger.info('Express Intialized');
        } catch (e) {
            logger.error('Express Intialization Failed');
        }
        return this.app;
    }
};