import express, {Application, Express} from "express";
import * as bodyParser from 'body-parser';
import cors from "cors";

export default ( app: Express ) : Express => {

    app.get('/status', (req , res ) => { res.status(200).end(); });
    app.head('/status', (req, res) => { res.status(200).end(); });
    app.enable('trust proxy');
    app.use(bodyParser.urlencoded({ extended: false }));
    // ...More middlewares
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(express.json()); // This is the line that you want to add
    var corsOptions = {
        origin: "*",
        optionsSuccessStatus: 200, // For legacy browser support
        methods: "GET, PUT"
    }
    app.use(cors(corsOptions))


    // Return the express app
    return app;
};