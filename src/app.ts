import loaders from "./loaders"
import config from "./config"
import express from "express"
import dotenv from "dotenv";
import {logger} from "./loaders/winston";
import {Routes} from "./routes/routes";
import {Express} from "express";
import Loader from "./loaders";
import CoverageService from "./service/coverage.service";
import {CronJob} from 'cron';

dotenv.config({path: 'src/config/.env'});

export default class App {
    public app : Express;
    public route: Routes;
    public loader: Loader;


    constructor() {
        this.app = express();
        this.route = new Routes();
        this.loader = new loaders(this.app);
    }

    public async start(): Promise<void> {
        this.app = this.loader.init();
        this.app.listen(config.port, () => {
            this.route.routes(this.app);

            new CronJob('0 * * * *', () => {
                logger.info('running a task every minute.');
                new CoverageService().coverage();
            }).start();



            logger.info('Server running on port ' + config.port);
        });
    }
}

new App().start()

