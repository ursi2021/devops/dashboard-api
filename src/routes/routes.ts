import {Express, NextFunction, Router} from "express";
import {CoverageController,} from "../controller/coverage.controller";

export class Routes {



    constructor() {

    }

    public routes(app: Express): void {
        app.route("/").get(CoverageController.getCoverage);
        app.route("/update").get(CoverageController.updateCoverage);
    }
}
